# Knowledge & Experience Questions

- [x] Tantangan terbesar saat saya diberikan tanggung jawab untuk membuat aplikasi yang workflownya tidak jelas. Jadi saya harus mencari pendekatan dari aplikasi-aplikasi yang sudah ada. sembari melakukan konsultasi kepada pihak atasan.
- [x] Dengan menerapkan Testing. Dan Mempelajari aturan-aturan yang di terapkan PSR
- [x] Dengan memecah setiap flow menjadi branch sesuai kesepakatan. Sehingga bisa tetap fokus dan memiliki rasa tanggungjawab antar sesama tim.
- [x] Belum pernah menggunakan Design Pattern
- [x] Iya, Saya bersedia. Jika memang bisa remote untuk komunikasi saya ikut dari pihak kantor. waktu & availability saya juga dapat mengikuti dari kantor. Sesuai dengan kesepakatan.